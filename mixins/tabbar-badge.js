import {
	mapGetters
} from 'vuex'

export default {
	computed: {
		...mapGetters('cart', ['total'])
	},
	// onLoad() {
	// 	uni.setTabBarBadge({
	// 		index: 2,
	// 		text: this.total + ''
	// 	})
	// },
	// 只要每次显示都会根据vuex里的最新的购物车去设置徽标
	onShow() {
		if (this.total > 0) {
			uni.setTabBarBadge({
				index: 2,
				text: this.total + ''
			})
		} else {
			// 移除徽标
			uni.removeTabBarBadge({
				index: 2
			})
		}
	},
	watch: {
		total: {
			handler() {
				if (this.total > 0) {
					uni.setTabBarBadge({
						index: 2,
						text: this.total + ''
					})
				} else {
					// 移除徽标
					uni.removeTabBarBadge({
						index: 2
					})
				}
			},
			immediate: true
		}
	}
}
