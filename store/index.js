// 导入Vue
import Vue from 'vue'
// 导入Vuex
import Vuex from 'vuex'
// 安装Vuex
Vue.use(Vuex)

// 导入cart模块
import cart from './modules/cart.js'
import user from './modules/user.js'

// 实例化一个store对象
export default new Vuex.Store({
	modules: {
		cart,
		user
	}
})
