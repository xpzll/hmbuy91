export default {
	// 开启命名空间
	namespaced: true,
	state() {
		return {
			// 购物车是一个数组，数组里每个元素是一个对象
			// 对象里至少要存：商品id、商品名字、商品价格、商品数量、商品图片、商品是否选中
			cartList: uni.getStorageSync('cart') || []
		}
	},
	getters: {
		total(state) {
			// 统计出总数量--要统计出选中的数量
			let count = 0
			state.cartList.forEach(v => {
				if (v.goods_checked) {
					count += v.goods_count
				}
			})
			// 把统计的数量返回出去
			return count
		},

		// 统计总价
		totalPrice(state) {
			// 统计出总数量--要统计出选中的数量
			let price = 0
			state.cartList.forEach(v => {
				if (v.goods_checked) {
					price += v.goods_count * v.goods_price
				}
			})
			// 把统计的数量返回出去
			return price
		},

		checkAll(state) {
			// 统计每个商品的goods_checked是否为true,都为true才返回true，有一个是false就返回false，所以可以用every
			return state.cartList.every(v => v.goods_checked)
		}
	},
	mutations: {
		// 把要添加的商品传进来
		addGoods(state, goods) {
			// 判断当前商品在不在数组里
			// 左边的v代表当前数组里的每一项，右边的代表当前要添加的商品的id
			// 通过id来找，如果找到了会得到这个元素，没找到会得到undefined
			const item = state.cartList.find(v => v.goods_id === goods.goods_id)
			if (item) { // 只有0、空字符串、null、undefined、NaN会得到false，其他都是true
				// 如果在就只要让数量+1
				item.goods_count++
			} else {
				// 如果不在就是加到数组里
				state.cartList.push(goods)
			}

			// 把最新的购物车数据存到本地
			uni.setStorageSync('cart', state.cartList)
		},

		// 专门用来设置选中或不选中的方法
		setGoodsChecked(state, goods_id) {
			// 找到对应的数据
			const item = state.cartList.find(v => v.goods_id === goods_id)
			item.goods_checked = !item.goods_checked
			// 存到本地存储
			uni.setStorageSync('cart', state.cartList)
		},

		// 专门用来修改数量的方法
		// 因为既要传id，又要传数量，所以当一个对象传进来
		setGoodsCount(state, obj) {
			// 首先根据id找到要修改的商品
			const item = state.cartList.find(v => v.goods_id === obj.goods_id)
			item.goods_count = obj.goods_count
			// 存到本地存储
			uni.setStorageSync('cart', state.cartList)
		},

		// 删除商品
		delGoodsById(state, id) {
			// 删除一个元素其实就相当于过滤出来除了它以外的元素
			// 过滤出id不等于我要删的id的其他元素组成新数组
			state.cartList = state.cartList.filter(v => v.goods_id != id)
			uni.setStorageSync('cart', state.cartList)
		},

		setAllGoodsChekced(state, checked) {
			state.cartList.forEach(v => v.goods_checked = checked)
		}
	}
}
