export default {
	namespaced: true,
	state() {
		return {
			address: {}
		}
	},
	getters: {
		// 专门用来拼接地址的
		addressStr(state) {
			return state.address.provinceName + state.address.cityName + state.address.countyName + state.address
				.detailInfo
		}
	},
	mutations: {
		setAddress(state, addr) {
			state.address = addr
		}
	}
}
