import App from './App'

// 导入即可
import "./utils/request.js"
// 导入
import store from './store/index.js'

// 下面这种注释是一种特殊的注释，叫条件编译命令
// 作用：可以判断当前的环境，来执行不同的代码
// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	store,
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
