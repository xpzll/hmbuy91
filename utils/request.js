// 按需导入 $http 请求对象
import { $http } from '@escook/request-miniprogram'

// 将按需导入的 $http 挂载到 wx 顶级对象之上，方便全局调用
// wx这个东西是微信小程序里的顶级对象，也就是说所有页面都可以访问wx这个对象
// 所以我所有地方就可以用 wx.$http访问到请求对象了
// wx.$http = $http

// 设置基地址
$http.baseUrl = 'https://api-hmugo-web.itheima.net/api/public/v1/'

// 在 uni-app 项目中，可以把 $http 挂载到 uni 顶级对象之上，方便全局调用
// 小程序里wx是顶级对象，但是在uniapp中，uni才是顶级对象
uni.$http = $http

// 请求开始之前做一些事情
// 因为咱们是uniapp项目，顶级对象不叫wx，叫uni，记得把wx这个改成uni
$http.beforeRequest = function (options) {
  uni.showLoading({
    title: '数据加载中...',
  })
}

// 请求完成之后做一些事情
$http.afterRequest = function () {
  uni.hideLoading()
}